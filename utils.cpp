#include "utils.hpp"

/* utils.cpp 
Any and all protected, static subroutines shared by the other files. */

/* errno_kill
A super duper simple function so I can stop writing more or less the same two
lines every time we error check. --Rory
The string would be a char* but C++ freaks out. Dunno why.
@params -   string id: an identifier for code location
            boolean kill: should this kill?
@returns void */
void errno_kill(const char* id, bool kill){
    fprintf(stderr, "ERROR_%s: %d\n%s\n", id, errno, strerror(errno));
    if(kill) exit(EXIT_FAILURE);
}