#ifndef SERVER_H
#define SERVER_H

#include <vector>
#include <sys/types.h> // socket, bind, listen, accept, connect, send, recv
#include <sys/socket.h> /// socket, bind, listen, accept, connect, send, recv
#include <fcntl.h> // used for open()
#include <unistd.h> // used for read(), write(), close()
#include <errno.h> // used for perror()
#include <netdb.h> // socket data structure
#include <string.h>
#include <regex>          // used for name testing
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "utils.hpp"
#include "Lock_Manager.hpp"
#include "Worker.hpp"

#define BACK_LOG        500
// Threads
#define DEFAULT_THREAD_COUNT 4
#define DEFAULT_REDUNDANCY 0
#define SLEEP_TIME 10

/* Server.hpp
Defines the class "Connection".
Connection contains information critical to the server such as IP and port. */
class Server{
private:
    // Server Socket
    int server_fd; // file descriptor. The actual socket
    struct sockaddr_in server_addr; // server address w/ socket name, ip, port, protocol

    // Client Socket
    int client_fd;
    struct sockaddr client_addr;
    socklen_t client_addr_len; // accept() is pass by reference so we can't use sizeof(client_addr)

    // Threads
    int total_thread_count;   // Default = DEFAULT_THREAD_COUNT
    std::vector<Worker*> workers;

    // Locks
    Lock_Manager* locker;

    // File Redundancy
    bool is_redundant;

public:
    /*--------------------------------------------------------------------------
    Constructors
    --------------------------------------------------------------------------*/
    Server(const char* new_ip, 
           const char* new_port = DEFAULT_PORT, 
           const int new_thread_count = DEFAULT_THREAD_COUNT,
           const bool new_is_redundant = DEFAULT_REDUNDANCY);

    Server(const Server &source); // copy constructor

    /*--------------------------------------------------------------------------
    Destructor
    --------------------------------------------------------------------------*/
    ~Server();

    /*--------------------------------------------------------------------------
    Operators
    --------------------------------------------------------------------------*/
    Server& operator=(const Server &source);

    /*--------------------------------------------------------------------------
    Helper Functions
    --------------------------------------------------------------------------*/
private:
                void    copy(const Server &source);
                // Server Setup
    unsigned    long    get_addr(const char* name_in);
                void    create_socket(const char* ip, const char* port);
                // MAIN DISPATCH LOOP
                void    dispatch();
                void    init_workers(int worker_count);
};

#endif
