Written by David Wu (DamWu@ucsc.edu) and Rory Landau (rflandau@ucsc.edu).

# Overview
Httpserver is a simple server that responses to GET and PUT requests for files
with multithreading and redundancy capabilities. Its architecture is described in DESIGN.pdf.

# Compiling and Running
Httpserver can and should be compiled using the `make` command.
Directories "copy1", "copy2", "copy3" should exist in source directory before execution.

It can then be run via `./httpserver <ip> <port> -r -N <thread>`. Port is optional and defaults to 80.
-r: Flag for enabling file redundancy. Default false.
-N <thread>: Number of worker threads to run in parallel.


# Capabilities and Limitations
Unable to handle binary files if client requests a PUT and mixes header with file data.

# Style Guide

* Indentation is soft 4.

* Variable declaration, other than explicitly temporary variables, should be performed at the start of the variable's subroutine.

* Lines, ideally, should not exceed 80 characters and definitely no longer than 100 characters.
