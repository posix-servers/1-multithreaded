#include "Server.hpp"
#include "Lock_Manager.hpp"

using namespace std;

/*------------------------------------------------------------------------------
Notes
------------------------------------------------------------------------------*/
// 0 is Standard Input
// 1 is Standard Output
// 2 is Standard Error


/*------------------------------------------------------------------------------
MAIN
------------------------------------------------------------------------------*/
int main (int argc, char** argv) {
    // variables
    char*               domain = NULL;
    char*               port   = NULL;
    int                 workerc = DEFAULT_THREAD_COUNT; // worker count
    int                 redun   = DEFAULT_REDUNDANCY;   // boolean, redundancy

    // handle arguments---------------------------------------------------------
    int opt;
    while ((opt = getopt(argc, argv, "N:r")) != -1) {
        switch (opt) {
            case 'N': // worker count specified
                workerc = atoi(optarg);
                break;
            case 'r': // use redundancy
                redun = 1;
                break;
            default: // unknown!
            fprintf(stderr, "Usage: httpserver [-N workers] [-r] <domain> [<port>]\n");
            exit(EXIT_FAILURE);
        }
    }
    // check remaining argument count
    int noptargs = argc - optind;
    switch(noptargs){
        case 2: // domain and port were given, note the fall-through
            port = argv[optind+1];
        case 1: // domain was given
            domain = argv[optind];
            break;
        default: // too many or too few args remained
            fprintf(stderr, "Too %s non-option arguments given.\n"
            "Usage: httpserver [-N workers] [-r] <domain> [<port>]\n",
            (noptargs == 0) ? "few" : "many"); // single-word ternary
            exit(EXIT_FAILURE);
    }
    // initalize server class---------------------------------------------------
    //Server serv = (port == NULL) ? Server(domain) : Server(domain, port); // RELIC
    Server serv = (port == NULL) ?
        Server(domain, DEFAULT_PORT, workerc, redun):
        Server(domain, port, workerc, redun);
}
